
build: src/dalsa_gige_grant.cpp CMakeLists.txt
	mkdir -p build && cd build && cmake .. && make

install:
	echo "Installing binary to /usr/local/bin;  command will run as sudo."
	cd build && sudo install --owner=root --group=root --mode=4755 dalsa_gige_grant /usr/local/bin

.PHONY: build install
