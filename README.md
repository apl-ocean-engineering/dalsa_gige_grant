# dalsa_gige_grant

The [dalsa_gige_driver](https://gitlab.com/apl-ocean-engineering/dalsa_gige_driver) 
driver requires two privileged capabilties: `cat_net_raw` and `cap_sys_nice`.  
The simplest way to get these privileges is to run the driver under `sudo` 
but this is inconvenient from within a Roslaunch.

The `dalsa_gige_grant` binary grants the relevant privileges to a binary 
passed on the command line.  It **must run as setuid root.** 

In general is **should not** be included in a ROS/Catkin build;  it should 
be built standalone and installed once per machine.  It only needs to be 
installed once.  

## Installation

`dalsa_gige_grant` should be built with the standard `cmake` toolchain:

1. Build with cmake:

```
mkdir build
cd build
cmake ..
make
```

2.  Install to `/usr/local/bin` **with setuid**:

```
sudo install --owner=root --group=root --mode=4755 dalsa_gige_grant /usr/local/bin
```

A convenience `Makefile` with targets `build` and `install` runs the above steps. 


