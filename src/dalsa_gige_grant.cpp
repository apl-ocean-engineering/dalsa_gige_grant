/**
 * This code is taken from ethercat_grant, which takes it from pr2-grant
 */

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/capability.h>
#include <string>
#include <cstdlib>
#include <sys/prctl.h>

using namespace std;

#define EXECUTABLE "/var/tmp/granted"

int main(int argc, char *argv[])
{
  // Remove old executable, if it exists
  unlink(EXECUTABLE);

  // Copy executable, specific on command line, to /var/tmp
  string cmd;
  cmd = string("cp ") + string(argv[1]) + string(" " EXECUTABLE);
  if (system(cmd.c_str()) == -1) {
    perror("cp");
    return -1;
  }
  if (chown(EXECUTABLE, getuid(), getgid()) < 0) {
    perror("chown");
    return -1;
  }

  // Create enhanced capability set
  //
  // During testing in docker, cap_dac_override was required for non-root users
  // cap_net_raw allows for higher network performance (per Dalsa documentation)
  // cap_sys_nice allows the process to pin itself to a CPU
  //
  const char *cap_text = "cap_dac_override=ep cap_net_raw=ep cap_sys_nice=ep";
  cap_t cap_d = cap_from_text(cap_text);
  if (cap_d == NULL) {
    perror("cap_from_text");
    return -1;
  }

  // Set file capability
  int retval = cap_set_file(EXECUTABLE, cap_d);
  if (retval != 0) {
    fprintf(stderr, "Failed to set capabilities on file `%s' (%s)\n", argv[1], strerror(errno));
    return -1;
  }

  // Free capability
  if (cap_d) {
    cap_free(cap_d);
  }

  // Drop privileges to original caller
  retval = setuid(getuid());
  retval = setgid(getgid());

  // Allow core dumps
  prctl(PR_SET_DUMPABLE, 1, 0, 0, 0);

  // Preserve environment from caller (which should include LD_LIBRARY_PATH to the ROS libraries)
  extern char** environ;

  if (execve(EXECUTABLE, argv + 1, environ) < 0) {
    perror("execv");
    return -1;
  }

  return 0;
}
